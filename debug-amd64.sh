set -euxo pipefail
source ~/rump/rumprun.amd64/obj-amd64-hw/config-PATH.sh
cd ~/rump/ls
# -s -S for use as remote gdb on localhost:1234
rumprun qemu -i ls.baked.amd64 -g "-hda hdaamd64.qcow2 -s -S" & disown
