set -euxo pipefail
source ~/rump/rumprun.amd64/obj-amd64-hw/config-PATH.sh
cd ~/rump/ls

# -g "-display gtk"
#rumprun qemu -i ls.baked.amd64 -g "-hda hdaamd64.qcow2"
./rumprun.amd64 qemu -i -b ~/rump/netbsd.amd64/hdd.qcow2,/mnt -M 128 ls.baked.amd64


