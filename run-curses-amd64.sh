set -euxo pipefail
source ~/rump/rumprun.amd64/obj-amd64-hw/config-PATH.sh
cd ~/rump/ls

# virtio creates /dev/ld something devices
#qemu-system-x86_64 -m 128 -kernel ls.baked.amd64 \
# -drive file=hdaamd64.qcow2,format=raw \
#  -nographic -serial mon:stdio -append 'console=ttyS0'

./rumprun.amd64 qemu -b ~/rump/netbsd.amd64/hdd.qcow2,/mnt -M 128 ls.baked.amd64


#   -drive if=virtio,file=hdaamd64.qcow2,format=raw \
# -hda hdaamd64.qcow2