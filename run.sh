set -euxo pipefail

cd ~/rump/ls
# There is -bios and -kernel.
# -kernel does not work for palcode-clipper, so I shouldn't be afraid of trying -bios.  Even though rump for x86 uses -kernel.

~/QEMU/alpha-softmmu/qemu-system-alpha -hda image/hd.img -cdrom NetBSD-7.1.2-amd64.iso -bios ls.baked.alpha -m 128 & disown
